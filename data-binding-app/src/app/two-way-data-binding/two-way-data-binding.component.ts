import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-two-way-data-binding',
  templateUrl: './two-way-data-binding.component.html',
  styleUrls: ['./two-way-data-binding.component.scss']
})
export class TwoWayDataBindingComponent implements OnInit {

  name1: string = "Olá";
  name2: string;

  client = {
    firstName: "John",
    lastName: "Mack",
    address: "",
    age: 20
  }

  constructor() { }

  ngOnInit() {
  }

}
