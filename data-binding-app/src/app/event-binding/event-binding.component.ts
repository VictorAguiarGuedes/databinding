import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-event-binding',
  templateUrl: './event-binding.component.html',
  styleUrls: ['./event-binding.component.scss']
})
export class EventBindingComponent implements OnInit {

  buttonName: string = "My button";
  i: number = 0;
  spinnerMode = "determinate";
  spinnerValue = 0;
  btnEnable = true;
  selectDisable = false;
  selectedOption = 1;
  inputName = "John";

  constructor() { }

  ngOnInit() {
  }

  save() {
    console.log("click");
  }

  inc() {
    this.i++;
    this.buttonName = "It was clicked!! " + this.i + " vezes";
  }

  inc2() {
    this.spinnerValue = (this.spinnerValue + 10) % 100;
  }

  disable() {
    this.btnEnable = false;
    this.spinnerMode = "indeterminate";
    setTimeout(() => {
      this.btnEnable = true;
      this.spinnerMode = "determinate";
    }, 3000);
  }

  cbChange(event) {
    console.log(event.checked);
    this.selectDisable = event.checked;
  }

  selectionChange(event) {
    console.log(event);
    this.selectedOption = event.value;
  }

  /*  Não é mais necessário
  inputEvent(event) {
    console.log(event.target.value);
    console.log(this.inputName);
  }
  */

}
